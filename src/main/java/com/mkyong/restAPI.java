package com.mkyong;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@PropertySource(value = { "classpath:application.properties" })
@RequestMapping("/rest")
public class restAPI {
	
	 private static final String FILE_PATH = "/tmp/example.pdf";
	 private static final String APPLICATION_PDF = "application/pdf";
	
	
	@RequestMapping(value = "/restExportExcel", method = RequestMethod.GET, produces = APPLICATION_PDF)
	public String restExportExcel(HttpServletResponse response) throws IOException {

		XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("Datatypes in Java");
        Object[][] datatypes = {
                {"Datatype", "Type", "Size(in bytes)"},
                {"int", "Primitive", 2},
                {"float", "Primitive", 4},
                {"double", "Primitive", 8},
                {"char", "Primitive", 1},
                {"String", "Non-Primitive", "No fixed size"}
        };

        int rowNum = 0;
        System.out.println("Creating excel");

        for (Object[] datatype : datatypes) {
            Row row = sheet.createRow(rowNum++);
            int colNum = 0;
            for (Object field : datatype) {
                Cell cell = row.createCell(colNum++);
                if (field instanceof String) {
                    cell.setCellValue((String) field);
                } else if (field instanceof Integer) {
                    cell.setCellValue((Integer) field);
                }
            }
        }

        FileOutputStream outputStream = null;
        String outputFile = "";
        try {
          /* outputStream = new FileOutputStream("");
            workbook.write(outputStream);
            workbook.close();*/
        	
        	
        	Date d = new Date();
        	SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");  
 	        String dateNowStr = simpleDateFormat.format(d);  
        	
        	outputFile = "D:\\igasExcel\\" + dateNowStr +".xls";
        	
        	FileOutputStream fOut = new FileOutputStream(outputFile);
        	
        	/*response.setContentType("application/octet-stream");
            response.setHeader("Content-disposition", "attachment;filename=createList.xls");//默认Excel名称
            response.flushBuffer();*/
            
            workbook.write(fOut);
            //workbook.write(response.getOutputStream());
        	
            
        	
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("Done");
        return outputFile;
        
        
        
      
	

		       /* File file = getFile();
		        InputStream in = new FileInputStream(file);

		        response.setContentType(APPLICATION_PDF);
		        response.setHeader("Content-Disposition", "attachment; filename=" + file.getName());
		        response.setHeader("Content-Length", String.valueOf(file.length()));
		        FileCopyUtils.copy(in, response.getOutputStream());*/
		
        
	
	}
	
	/*private File getFile() throws FileNotFoundException {
        File file = new File(FILE_PATH);
        if (!file.exists()){
            throw new FileNotFoundException("file with path: " + FILE_PATH + " was not found.");
        }
        return file;
    }*/
	
}
