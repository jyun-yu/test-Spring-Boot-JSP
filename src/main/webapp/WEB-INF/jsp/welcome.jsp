<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html lang="en">
<head>

<link rel="stylesheet" type="text/css"
	href="webjars/bootstrap/3.3.7/css/bootstrap.min.css" />


<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	<link rel="stylesheet" href="http://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<script src="http://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="http://canvasjs.com/assets/script/canvasjs.min.js"></script>

	<link href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,700' rel='stylesheet' type='text/css'>

	<link rel="stylesheet" href="<c:url value='/resources/css/admin/style.css'/>"> <!-- Resource style -->
<script src="<c:url value='/resources/js/modernizr.js'/>"></script> <!-- Modernizr -->
<script src="<c:url value='/resources/js/jquery.menu-aim.js'/>"></script>
<script src="<c:url value='/resources/js/main.js'/>"></script> <!-- Resource jQuery -->
<script src="http://jstepper.emkay.dk/scripts/jquery.jstepper.js"></script>
<script src="<c:url value='/resources/js/common.js'/>"></script> <!-- Resource jQuery -->



<!-- Resource jQuery -->
	<script>
		$.ajaxSetup({
			// Disable caching of AJAX responses
			cache: false
		});
		/* $(function() {
			$("#topbanner").load("topbanner.html");
			$("#sidebanner").load("sidebanner.html");
		}); */
	</script>
	<script src='https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyBwH0V290P4KT5lPSjbRujwbruFnY1chHc'></script>
<!-- 
	<spring:url value="/css/main.css" var="springCss" />
	<link href="${springCss}" rel="stylesheet" />
	 -->
<c:url value="/css/main.css" var="jstlCss" />
<link href="${jstlCss}" rel="stylesheet" />

</head>
<body>

	<nav class="navbar navbar-inverse">
		<div class="container">
			<div class="navbar-header">
				<a class="navbar-brand" href="#">Spring Boot</a>
			</div>
			<div id="navbar" class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li class="active"><a href="#">Home</a></li>
					<li><a href="#about">About</a></li>
				</ul>
			</div>
		</div>
	</nav>

	<div class="container">

		<div class="starter-template">
			<h1>Spring Boot Web JSP Example</h1>
			<h2>Message: ${message}</h2>
			
			<button type="button" class="btn" id="exportExcel">匯出Excel</button></p>
		</div>

	</div>
	<!-- /.container -->

	<script type="text/javascript"
		src="webjars/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</body>

</html>

<script>

	$("#exportExcel").click(function(){
		
		alert("click");
		
		$.ajax({
			//url : 'https://www.google.com.tw/',
			url : '../test/rest/restExportExcel',
			method : 'GET',
			async : true,
			contentType: "text/plian; charset=utf-8",
			dataType:'text',
			
			// code to run if the request succeeds;
			// the response is passed to the function
			success : function(response) {
				
			
				alert('請求成功:  ' + response);
				
				/*var xmlhttp = new XMLHttpRequest();
				xmlhttp.open("GET",response,true);
				xmlhttp.send();*/
				
				
			},

			// code to run if the request fails;
			// the raw request and status codes are
			// passed to the function
			error : function(xhr, status) {
				alert('Sorry, there was a problem!');
			},

			// code to run regardless of success or failure
			complete : function(xhr, status) {
				alert('The request is complete!');
			}
		});	
		
	});
	

	
</script>	
	
